const chromeLauncher = require('chrome-launcher');

chromeLauncher.launch({
    port: 9222,
    chromeFlags: ['--headless','--proxy-server=54.171.181.204:8888','--disable-web-security','--disable-gpu']
}).then(chrome => {
    console.log(`Chrome debugging port running on ${chrome.port}`);
});