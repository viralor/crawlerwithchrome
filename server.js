const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');

const app = express();
const PORT = 8080;


app.use(bodyParser.json());
app.use('/api',api);

app.get('/api',(req,res) => {
    res.send('Send POST REQUEST (NO GET REQUEST)')
})

app.get('/', (req,res) => {
    res.send('Uhh :(')
});

app.listen(PORT,() => {
    console.log('I am on')
})

