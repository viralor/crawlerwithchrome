FROM node:latest

RUN  apt-get update && \
        apt-get -y install sudo


RUN mkdir -p /src/app

WORKDIR /src/app

COPY package.json /src/app/package.json

RUN npm install

COPY . /src/app


CMD ["node","server.js"]