const CDP = require('chrome-remote-interface');
const {chromeIP,port} = require('../config')

class crawl {
    static getHTML(url,cb){

        CDP.New({'host':chromeIP,'port':port,'url':url},(err,target) => {
            if(!err){
                CDP({target},(client) => {
                    const {Network, Page, Runtime,DOM} = client;
                    Network.setUserAgentOverride({'userAgent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36'});
                    Network.enable();
                    Page.enable();
                    Runtime.enable();
                    Page.navigate({url});
                        Page.loadEventFired(() => {
                            Runtime.evaluate({
                                expression:`document.querySelectorAll('iframe')[1].contentDocument.all.adBlock.querySelector('table').innerHTML`,
                                returnValue:true})
                                .then(({result}) => {
                                    cb(null,{html:result.value})
                                    CDP.Close({host:chromeIP,port:port,id:target.id})
                                })
                        })


                })
            }else{
                cb(err,null)

            }
        });
    }
}

module.exports = crawl